#include <DAVE.h>               //Declarations from DAVE Code Generation (includes SFR declaration)
#include "debug.h"

/* Basic Registers */
#define REG_BMCR                (0x00U)        /* Basic Mode Control Register       */
#define REG_BMSR                (0x01U)        /* Basic Mode Status Register        */
#define REG_PHYIDR1             (0x02U)        /* PHY Identifier 1                  */
#define REG_PHYIDR2             (0x03U)        /* PHY Identifier 2                  */
#define REG_ANAR                (0x04U)        /* Auto-Negotiation Advertisement    */
#define REG_ANLPAR              (0x05U)        /* Auto-Neg. Link Partner Abitily    */
#define REG_ANER                (0x06U)        /* Auto-Neg. Expansion Register      */
#define REG_ANNPTR              (0x07U)        /* Auto-Neg. Next Page TX            */
#define REG_PHY_CTRL2           (0x1fU)        /* Phy Control 2 */

/* Operation Mode Strp Override */
#define REG_OMSO                (0x16U)

/* Extended Registers */
#define REG_PHYCTRL1            (0x1eU)        /* PHY control 1 Register            */

/* Basic Mode Control Register */
#define BMCR_RESET              (0x8000U)      /* Software Reset                    */
#define BMCR_LOOPBACK           (0x4000U)      /* Loopback mode                     */
#define BMCR_SPEED_SEL          (0x2000U)      /* Speed Select (1=100Mb/s)          */
#define BMCR_ANEG_EN            (0x1000U)      /* Auto Negotiation Enable           */
#define BMCR_POWER_DOWN         (0x0800U)      /* Power Down                        */
#define BMCR_ISOLATE            (0x0400U)      /* Isolate Media interface           */
#define BMCR_REST_ANEG          (0x0200U)      /* Restart Auto Negotiation          */
#define BMCR_DUPLEX             (0x0100U)      /* Duplex Mode (1=Full duplex)       */
#define BMCR_COL_TEST           (0x0080U)      /* Collision Test                    */

/* Basic Mode Status Register */
#define BMSR_100B_T4            (0x8000U)      /* 100BASE-T4 Capable                */
#define BMSR_100B_TX_FD         (0x4000U)      /* 100BASE-TX Full Duplex Capable    */
#define BMSR_100B_TX_HD         (0x2000U)      /* 100BASE-TX Half Duplex Capable    */
#define BMSR_10B_T_FD           (0x1000U)      /* 10BASE-T Full Duplex Capable      */
#define BMSR_10B_T_HD           (0x0800U)      /* 10BASE-T Half Duplex Capable      */
#define BMSR_MF_PRE_SUP         (0x0040U)      /* Preamble suppression Capable      */
#define BMSR_ANEG_COMPL         (0x0020U)      /* Auto Negotiation Complete         */
#define BMSR_REM_FAULT          (0x0010U)      /* Remote Fault                      */
#define BMSR_ANEG_ABIL          (0x0008U)      /* Auto Negotiation Ability          */
#define BMSR_LINK_STAT          (0x0004U)      /* Link Status (1=established)       */
#define BMSR_JABBER_DET         (0x0002U)      /* Jaber Detect                      */
#define BMSR_EXT_CAPAB          (0x0001U)      /* Extended Capability               */

/* PHY control 1 Register */
#define PHYCTRL1_OPMODE_SPEED   (0x0003U)
#define PHYCTRL1_OPMODE_DUPLEX  (0x0004U)

/* PHY Identifier Registers */
#define PHY_ID1                 (0x0022U)      /* KSZ8031 Device Identifier MSB */
#define PHY_ID2                 (0x1560U)      /* KSZ8031 Device Identifier LSB */

/* phy control 2 register */
#define PHYCTRL2_HP_MDIX_ENABLE  (0x1 << 15)

uint8_t phy_existence_tabel[8] = {0,0,0,0,0,0,0,0};

void dyc_phy_init()
{
    static int flag = 0;
    XMC_ECAT_STATUS_t rel1,rel2;
    uint16_t data1 = 0,data2 = 0;
    uint16_t reg;

    for(int i = 0;  i < 7;i++)
    {
        rel1 = XMC_ECAT_ReadPhy(i,REG_PHYIDR1,&data1);
        rel2 = XMC_ECAT_ReadPhy(i,REG_PHYIDR2,&data2);
        if(rel1 != XMC_ECAT_STATUS_OK || rel2 != XMC_ECAT_STATUS_OK ||\
            data1 != PHY_ID1 || data2 != PHY_ID2)
            DEBUG("PHYID %d No exit !!! \n",i);
        else
        {
            DEBUG("Read ADDR[%d],PHYIDR[%x]: 0x%x%x\n",i,REG_PHYIDR1,data1,data2);
            phy_existence_tabel[i] = 1;
        } 
        
    }
   
    /* reset phy */
    for(int i = 0 ; i < 7; i++)
    {
        if(phy_existence_tabel[i] == 1)
        {
            DEBUG("reset pyhid %d \n",i);
            rel1 = XMC_ECAT_WritePhy(i,REG_BMCR,BMCR_RESET);
            if(rel1 == XMC_ECAT_STATUS_OK)
            {
                uint16_t reg;
                do
                {
                    rel1 = XMC_ECAT_ReadPhy(i,REG_BMCR,&reg);
                    /* code */
                } while ((reg & BMCR_RESET) != 0);
            }
        }
    }
    
    /* set register omso */
    /*
    rel1 = XMC_ECAT_ReadPhy(0x1,REG_OMSO,&data1);
    if(rel1 != XMC_ECAT_STATUS_OK)
        printf("Read PHYIDR error \n");
    else
    {
        printf("Read ADDR[0x0],PHYIDR[%x]: 0x%x\n",REG_OMSO,data1);       
        data1 |= 0x1 << 9;
        rel1 = XMC_ECAT_WritePhy(0x1,REG_OMSO,data1);       
        if(rel1 != XMC_ECAT_STATUS_OK)
            printf("write PHYIDR error \n");
        else
            printf("Write ADDR[0],OMSO[%x]: 0x%x\n",REG_OMSO,data1);
    }
    */

    /*bmcr configuration*/
    #if 1
    {
        for(int i = 0; i < 7;i++)
        {
            if(phy_existence_tabel[i] == 1)
            {
                reg = 0;
                reg |= BMCR_SPEED_SEL;
                reg |= BMCR_DUPLEX;
                reg |= BMCR_ANEG_EN;
                rel1 = XMC_ECAT_WritePhy(i,REG_BMCR,reg);       
                if(rel1 != XMC_ECAT_STATUS_OK)
                    DEBUG("congigure phy[%d] error \n",i);
                else
                    DEBUG("Write ADDR[%d],BMCR[%x]: 0x%x\n",i,REG_BMCR,reg);
            }
        }

    }
    #endif

    #if 0
    {
        for(int i = 0 ; i < 8 ; i++)
        {
            if(phy_existence_tabel[i] == 1)
            {
                rel1 = XMC_ECAT_ReadPhy(i,REG_PHY_CTRL2,&data1);
                if(rel1 != XMC_ECAT_STATUS_OK)
                    printf("Read PHY[%d] REG_PHY_CTRL2[%d] error \n",i,REG_PHY_CTRL2);
                else
                {
                    printf("Read ADDR[%d],PHYIDR[%x]: 0x%x\n",i,REG_PHY_CTRL2,data1);       
                    
                    data1 &= ~(PHYCTRL2_HP_MDIX_ENABLE);
                    rel1 = XMC_ECAT_WritePhy(i,REG_PHY_CTRL2,data1);       
                    if(rel1 != XMC_ECAT_STATUS_OK)
                        printf("write PHYIDR error \n");
                    else
                        printf("Write ADDR[%d],REG_PHY_CTRL2[%x]: 0x%x\n",i,REG_PHY_CTRL2,data1);
                
                }
            }
        }
 
    }
    #endif

}

void dyc_phy_debug()
{
    static int flag = 0;
    XMC_ECAT_STATUS_t rel1,rel2;
    uint16_t data1 = 0,data2 = 0;
    uint16_t reg;

    if(flag == 0)
    {
   

        flag = 1;
    }


    {
        for(int i = 0 ; i < 8; i++)
        {
            if(phy_existence_tabel[i] == 1)
            {
                data1 = 0;
                rel1 = XMC_ECAT_ReadPhy(i,REG_BMCR,&data1);       
                if(rel1 != XMC_ECAT_STATUS_OK)
                    DEBUG("Read PhyAddr[%d] PHYIDR error \n",i);
                else
                    DEBUG("Read PHYADDR[%d],REGADDR[0],BMCR[%x]: 0x%x\n",i,REG_BMCR,data1);
            
                data1 = 0;
                rel1 = XMC_ECAT_ReadPhy(i,REG_BMSR,&data1);       
                if(rel1 != XMC_ECAT_STATUS_OK)
                    DEBUG("Read PhyAddr[%d],PHYBMSR error \n",i);
                else
                    DEBUG("Read PHYADDR[%d],REGADDR[0],BMSR[%x]: 0x%x\n",i,REG_BMSR,data1);
                
            }

        }
   
    }
}

/**************************************************************************************
 * 
 * 
 * ************************************************************************************/
#define ESC_PORT_DESCRIPTOR 0x0007

struct esc_port_descriptor
{
   uint8_t port0:2;
   uint8_t port1:2;
   uint8_t port2:2;
   uint8_t port3:2;
};


#define ESC_RX_ERR_CNT 0X0300

struct esc_rx_err_cnt_t
{
    uint8_t invalid_fram;
    uint8_t rx_err_cnt;
};

void dyc_esc_debug()
{
    /* esc rx err cnt */
    /*
    struct esc_rx_err_cnt_t rxErrCnt ;
    rxErrCnt.invalid_fram = 0;
    rxErrCnt.rx_err_cnt = 0;
    HW_EscRead(&rxErrCnt,ESC_RX_ERR_CNT + 0*sizeof(struct esc_rx_err_cnt_t),sizeof(struct esc_rx_err_cnt_t));
    DEBUG("[port 0]: invalid frame %d,rx error cnt %d\n",rxErrCnt.invalid_fram,rxErrCnt.rx_err_cnt);
    HW_EscRead(&rxErrCnt,ESC_RX_ERR_CNT + 1*sizeof(struct esc_rx_err_cnt_t),sizeof(struct esc_rx_err_cnt_t));
    DEBUG("[port 1]: invalid frame %d,rx error cnt %d\n",rxErrCnt.invalid_fram,rxErrCnt.rx_err_cnt);
    */

    /* port descriptor */
    /*
    struct esc_port_descriptor portDes;
    HW_EscRead((void*)&portDes,ESC_PORT_DESCRIPTOR,sizeof(struct esc_port_descriptor));
    DEBUG("port descriptor ,port0:%x,port1:%x,port2:%x,port3:%x\n",\
        portDes.port0,portDes.port1,portDes.port2,portDes.port3);
   */ 
}

void dyc_init(void)
{
    dyc_phy_init();
}

void dyc(void)
{
    dyc_phy_debug();

    dyc_esc_debug();
}
